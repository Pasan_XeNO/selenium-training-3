package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.test.util.MainRunner;

public class UploadImagePage extends MainRunner {
	WebElement choosefile;
	
	
	public UploadImagePage (WebDriver driver) {
		super();
		this.choosefile = driver.findElement(By.id("photofile"));
		
	}
	
	private UploadImagePage action_Upload_Image() throws Exception{
		choosefile.sendKeys("C://success.png");
		return this;
	}
	
	public UploadImagePage Step_Upload_Image() throws Exception{
		return action_Upload_Image();
	}
}
