package com.test.pages;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.test.panels.Header;

import com.test.util.MainRunner;

public class AdminPage extends MainRunner{
	WebDriver driver;
	WebElement addbtn;
	WebElement delbtn;
	
	Header headerPanel;
	
	public AdminPage (WebDriver driver) {
		super();
		this.driver = driver;
		this.addbtn = driver.findElement(By.id("btnAdd"));
		this.delbtn = driver.findElement(By.id("btnDelete"));
		this.headerPanel = new Header(driver);
	}
	
	private AdminPage action_Check_And_Validate_Admin_Page_UI_Elements() throws Exception {
		Thread.sleep(2000);
		assertTrue("Add button validation", addbtn.isEnabled());
		assertFalse("Delete button validation", delbtn.isEnabled());
		return this;
	}
	
	public AdminPage Check_And_Validate_Admin_Page_UI_Elements() throws Exception {
		return action_Check_And_Validate_Admin_Page_UI_Elements();
	}
	
	private AddUser action_Click_Add_Button() throws Exception {
		addbtn.click();
		return new AddUser(driver);
	}
	
	public AddUser Step_Click_Add_Button() throws Exception {
		return action_Click_Add_Button();
	}
	
	public MyInfoPanel Step_Click_MyInfo_Button() throws Exception {
		this.headerPanel.action_Click_MyInfo_Button();
		return new MyInfoPanel(driver);
	}
	
	
}
