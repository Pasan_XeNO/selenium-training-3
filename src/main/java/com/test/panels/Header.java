package com.test.panels;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.test.util.MainRunner;

public class Header extends MainRunner {
	WebDriver driver;

	WebElement btnMktPlace;
	WebElement lnkWelcome;
	WebElement lnkLogout;
	WebElement adminMenu;
	WebElement myInfoMenu;
	
	public Header(WebDriver driver) {
		super();
		this.driver = driver;
		this.btnMktPlace = driver.findElement(By.id("MP_link"));
		this.lnkWelcome = driver.findElement(By.id("welcome"));	
		this.adminMenu = driver.findElement(By.id("menu_admin_viewAdminModule"));	
		this.myInfoMenu = driver.findElement(By.id("menu_pim_viewMyDetails"));
	}
	
	public void action_Check_And_Validate_Header_UI_Elements() {
		assertTrue("Header Marketplace button validation", btnMktPlace.isEnabled());
		assertTrue(lnkWelcome.getText().contains("Welcome"), "Header Welcome text validation");
		assertEquals(adminMenu.getText(), "Admin", "Header Admin tab validation");
		assertEquals(myInfoMenu.getText(), "My Info", "Header My Info tab validation");
	}
	
	public void action_Navigate_To_Admin_Menu() {
		adminMenu.click();
		
	}
	
	public void action_Click_MyInfo_Button() {
		myInfoMenu.click();
	}
	
}
